
import AppSyncConfig from './AppSy';
import AwsAmplifyConfig from './AppSyncConfig';
import AWSAppSyncClient from 'aws-appsync';
import { Rehydrated } from 'aws-appsync-react';
import * as AWS from 'aws-sdk';
import { ApolloProvider } from 'react-apollo';
import React, { Component } from 'react';
import Amplify, { Auth } from 'aws-amplify';
import Routes from './routes'

Amplify.configure(AwsAmplifyConfig);  
class ApolloIndex extends Component {
    constructor(props) {
        super(props);
        this.state = {
            
        };

        
    }

    render() {
        const EndPoint = process.env.REACT_APP_ENDPOINT;
        const Region = process.env.REACT_APP_REGION;
// console.log('end point',pro)
        const client = new AWSAppSyncClient({
            url:AppSyncConfig.graphqlEndpoint,
        
            region: AppSyncConfig.region,
            auth: {
                type: AppSyncConfig.authenticationType,
                apiKey: AppSyncConfig.apiKey,
                // jwtToken: signInUserSession.idToken.jwtToken
                jwtToken: async () =>
                    (await Auth.currentSession()).getIdToken().getJwtToken()
                
            },
            disableOffline: true,
            complexObjectsCredentials: () => {
                return new AWS.Credentials({
                    accessKeyId: AppSyncConfig.accessKeyId,
                    secretAccessKey: AppSyncConfig.secretAccessKey
                });
            }
        });
        return (
            <ApolloProvider client={client}>
                <Rehydrated>
                   <Routes/>

                </Rehydrated>
            </ApolloProvider>
        );
    }
}


// export default withAuthenticator(ApolloIndex,false,[
//     <SignIn />,
// 	<ConfirmSignIn />,
// 	<VerifyContact />,
// 	<ForgotPassword />,
// 	<RequireNewPassword />
// ]);
export default ApolloIndex;

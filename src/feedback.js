import React,{Component} from 'react';
import { Button, Col,Form, Rate, Row,Radio, message  } from 'antd'
import FeedbackMutate from '../src/Mutation/feedback' 
import {withApollo} from 'react-apollo'

 class CreateForm extends Component{
  

  constructor(props){
    super(props)
    this.state={
        Message:false,
        loading: false,
        isError:false,
        isSuccess: false,
        link: '',
        innovative:'',
        brandLoyalty:'',
        brandAttributes:'',
        awareness:'',
        spaceOfEV:'',
        globalCompany:'',
        bestInTheWorld:'',
        error:false
    }
  }

  enterLoading = ()=>{
    this.setState({loading:true})
}

onRadioClick = (val,name)=>{
    this.setState({[name]:val.target.value})
}




handleSubmit=(e)=>{
    e.preventDefault();


    if((this.state.brandAttributes == "" || this.state.brandLoyalty == "" || this.state.awareness  == "" || this.state.innovative == "" || this.state.spaceOfEV == "" || this.state.globalCompany == "" || this.state.bestInTheWorld == "" )){
        this.setState({error: true})
    }else{

    
        this.enterLoading()
        try{
        this.props.client.mutate({
            mutation:FeedbackMutate,
            variables: {
                awareness: this.state.awareness,
                innovative: this.state.innovative,
                brandAttributes: this.state.brandAttributes,
                brandLoyalty: this.state.brandLoyalty,
                spaceOfEV: this.state.spaceOfEV,
	              globalCompany:this.state.globalCompany,
                bestInTheWorld: this.state.bestInTheWorld,
                eventName:"feedback-mr3"
            }
        }).then(({data}) => {
            // return false
            
            if(data.createFeedback ){
              message.success("Thank you. Your feedback has been recorded", 5)
              this.setState({
                loading:false,
                awareness:'',
                innovative:'',
                brandAttributes:'',
                brandLoyalty:'',
                spaceOfEV: '',
	              globalCompany: '',
                bestInTheWorld:'',
                error:false
              })
            }
            })
            .catch(err =>{
                message.info("Please give some feedback", 3)
                this.setState({
                    loading:false,
                  })
            });
       
        }
        catch(err){
            this.setState({
                loading:false,
              })
        }
      }
        
      
}

onRateChange = (val, name)=>{
  this.setState({[name]:val})
}


  render()
  {
    console.log(this.state.error)
   
return (
    <div>

        <Row >
            <Col xs={{span:0}} sm={{span:20}} md={{span:4}} lg={{span:8}}></Col>
                
                <Col xs={{span:20}} sm={{span:20}} md={{span:8}} lg={{span:8}}>

                    <Row>
                    <img src={require("../src/img/Mahindra Racing Logo-White Text.png")} 
                     style={{  width: '272px',height:'69px',marginLeft:'15%',marginBottom:'10px'}} />
                    </Row>
                </Col>
            <Col xs={{span:0}} sm={{span:20}} md={{span:4}} lg={{span:8}}></Col>
        </Row>  




     <Form onSubmit={this.handleSubmit} >
        <div>

            <Row >
                {/* <Col  xs={{span:24}} sm={{span:24}} md={{span:8}} lg={{span:8}} xl={{span:8}} > */}
                <Col xs={{span:2}} sm={{span:20}} md={{span:4}} lg={{span:8}}></Col>
                
                <Col xs={{span:20}} sm={{span:20}} md={{span:8}} lg={{span:8}}>

                   
                    <Row style={{marginTop:'20px'}}>
                        <div style={{color:"white", marginBottom:'6px', fontSize:'17px', fontWeight:500}} required="true" >1. Were you aware about Mahindra Racing’s participation in Formula E before today?</div>
                          <Radio.Group value={this.state.awareness} onChange={(e)=>this.onRadioClick(e,'awareness')}>
                          <Radio style={{color:'white'}} value={"YES"}>YES</Radio>
                          <Radio  style={{color:'white'}} value={"NO"}>NO</Radio>
                          </Radio.Group>
                    </Row>


                    <Row>
                     <div style={{color:"white", marginTop:'20px', marginBottom:'6px',  fontSize:'17px', fontWeight:500}} required="true" >2. Which words would you use to describe “Mahindra Racing”?</div>
                        
                          <Radio.Group value={this.state.brandAttributes} onChange={(e)=>this.onRadioClick(e,'brandAttributes')}>
                            <Radio style={{color:'white', fontSize:'15px'}} value={"Cutting Edge Technology"}>Cutting Edge Technology</Radio><br/>
                            <Radio  style={{color:'white', fontSize:'15px'}} value={"Superior design"}>   Superior design</Radio><br/>
                            <Radio  style={{color:'white', fontSize:'15px'}} value={"Sustainable Mobility"}>    Sustainable Mobility</Radio><br/>
                            <Radio  style={{color:'white', fontSize:'15px'}} value={"Thrilling and Exciting"}>  Thrilling and Exciting</Radio><br/>
                            <Radio  style={{color:'white', fontSize:'15px'}} value={"None"}>  None</Radio>
                          </Radio.Group>
                    </Row>

                    <Row>
                   <div style={{color:"white",marginTop:'20px', marginBottom:'6px',  fontSize:'17px', fontWeight:500}} required="true" >3.Rate the following statements on the scale of 1 to 5 :</div>
                          {/* <Rate  value={this.state.experience} count={10}  onChange={(val)=>this.onRateChange(val,'experience')}/> */}
                          <div style={{color:"white",marginTop:'8px',marginLeft:'20px', marginBottom:'6px',  fontSize:'17px', fontWeight:500}}>1. Mahindra is innovative
                          <div><Rate  value={this.state.innovative}  onChange={(val)=>this.onRateChange(val,'innovative')}/></div>
                          </div>
                          <div style={{color:"white",marginTop:'8px',marginLeft:'20px', marginBottom:'6px',  fontSize:'17px', fontWeight:500}}>2. Mahindra is active in the space of EVs
                          <div><Rate  value={this.state.spaceOfEV}  onChange={(val)=>this.onRateChange(val,'spaceOfEV')}/></div>
                          </div>
                          <div style={{color:"white",marginTop:'8px',marginLeft:'20px', marginBottom:'6px',  fontSize:'17px', fontWeight:500}}>3. Mahindra is a global company
                          <div><Rate  value={this.state.globalCompany}  onChange={(val)=>this.onRateChange(val,'globalCompany')}/></div>
                          </div>
                          <div style={{color:"white",marginTop:'8px',marginLeft:'20px', marginBottom:'6px',  fontSize:'17px', fontWeight:500}}>4. Mahindra can compete against the best in the world

                          <div><Rate  value={this.state.bestInTheWorld}  onChange={(val)=>this.onRateChange(val,'bestInTheWorld')}/></div>
                          </div>
                    </Row>

                    <Row>
                          <div style={{color:"white",  marginTop:'20px', marginBottom:'6px',  fontSize:'17px', fontWeight:500}} required="true" >4. How likely are you to recommend to follow this brand to a friend or colleague:</div>
                          <Rate  value={this.state.brandLoyalty} count={10}  onChange={(val)=>this.onRateChange(val,'brandLoyalty')}/>
                    </Row>



                    <Row >
                      <div  style={{ marginTop:'20px' ,  color:"red" }}>{this.state.error ? '*All the questions are mandatory.' : ''}</div>
                    <Form.Item >
                        <Button
                        type="primary"  htmlType="submit" 
                       loading={this.state.loading} style={{ marginTop:'20px' , marginLeft:'40%', backgroundColor:'#fc033d', color:"white",borderColor:'white' }} >
                         Submit
                        </Button>
                        {/* disabled={(this.state.brandAttributes != "" && this.state.brandLoyalty != "" && this.state.awareness  != "" && this.state.innovative != "" && this.state.spaceOfEV != "" && this.state.globalCompany != "" && this.state.bestInTheWorld != "" ) ? false : true}  */}
                    </Form.Item>
                    </Row>

                    <Row>
                    <img src={require("../src/img/IMG_20191209_163626.JPG")} 
                    style={{  width: '100%',height:'100%'}} />

                    </Row>


                 </Col>

                <Col xs={{span:2}} sm={{span:20}} md={{span:4}} lg={{span:8}}></Col>

            </Row>
        </div>
     </Form>

</div>
    )
  }
}

const WrapFeedBack = Form.create()(CreateForm);


export default withApollo(WrapFeedBack)






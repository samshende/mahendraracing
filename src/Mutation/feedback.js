import gql from 'graphql-tag'

export default gql`
mutation createFeedback(
    $awareness: String!
    $innovative: Int
    $spaceOfEV: Int
    $globalCompany: Int
    $bestInTheWorld: Int
    $brandAttributes: String!
    $brandLoyalty: Int!
    $eventName:String
  ){
    createFeedback(input:{
      awareness: $awareness
      brandAttributes: $brandAttributes
      brandLoyalty: $brandLoyalty
      innovative: $innovative
	    spaceOfEV: $spaceOfEV 
	    globalCompany: $globalCompany
      bestInTheWorld: $bestInTheWorld
      eventName:$eventName
    })
  }
`
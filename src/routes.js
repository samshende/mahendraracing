import React, { Fragment } from "react";
import FeedBackForm from '../src/feedback'
import { Route, BrowserRouter as Router } from "react-router-dom";
import CreateForm from './comp/CreateUserForm'
import regCount from './comp/regCount'


const RoutesComp = () =>{
    return(
        <Router>
            <Fragment>
                <Route exact path="/" component={(CreateForm)}/>
                <Route exact path="/feedback" component={(FeedBackForm)}/>
                <Route exact path="/stats1357" component={(regCount)}/>

            </Fragment>
        </Router>
    )
}

export default RoutesComp
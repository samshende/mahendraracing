import React,{Component} from 'react';
    import { Button, Col,Form, Input, Row, Select,Checkbox, message,Modal,Icon,InputNumber,Radio,Upload  } from 'antd'
    import './Page.css'
    import createUser from './createUser'
    import {withApollo} from 'react-apollo'
   import getCounts  from './getCount'
    
    const FormItem = Form.Item
    
    
     class regCount extends Component{
      
    
      constructor(props){
        super(props)
        this.state={
            data:{},
        }
        }




        componentDidMount(){

            this.props.client.query({
              query: getCounts,
            //   variables:{eventName: "user-mr3" },
              fetchPolicy: 'network-only',
            }).then(({data})=>{
            //   console.log('data cdm',JSON.parse(data.getCounts))
              this.setState({
                data:JSON.parse(data.getCounts),
              })
              
            }).catch((err)=>{
              console.log(err)
            })
          }
      


        render(){

            console.log("user",this.state.data)
            // console.log("feedback",this.state.data)
            
                return(
                <div>
                 <Row>
                <Col xs={{span:0}} sm={{span:20}} md={{span:4}} lg={{span:8}}></Col>
                    
                    <Col xs={{span:20}} sm={{span:20}} md={{span:8}} lg={{span:8}}>
                        <Row>
                        <img src={require("../img/Mahindra Racing Logo-White Text.png")} 
                         style={{  width: '272px',height:'69px',marginLeft:'15%',marginBottom:'10px'}} />
                        </Row>
                    </Col>
                <Col xs={{span:0}} sm={{span:20}} md={{span:4}} lg={{span:8}}></Col>
            </Row>  

            <Row>
                    <Col xs={{span:2}} sm={{span:20}} md={{span:4}} lg={{span:8}}></Col>
                    <Col xs={{span:2}} sm={{span:20}} md={{span:4}} lg={{span:8}}>
                        <Row>
                        <div style={{
                            width:'300px',
                            margin:'auto',

                            textAlign:'center',
                            boxShadow:'0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)'
                        }}>
                         <div style={{
                             backgroundColor:'#fc033d',
                             color:'white',
                             padding:'10px',
                             fontSize:'40px'
                         }}>
                          <h2 style={{color:'white'}}>{this.state.data.user ? this.state.data.user : 0}</h2>
                        </div>

                         <div style={{
                             padding:'10px',
                             color:'black',
                             backgroundColor:'white',
                             fontWeight:'800'
                             
                         }}>
                        <p >Total No. of Registrations</p>
                        </div>
                        </div>
                    </Row>

                    <Row>
                        <div style={{
                            width:'300px',
                            textAlign:'center',
                            margin:'auto',
                            marginTop:'20px',
                            boxShadow:'0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)'
                        }}>
                         <div style={{
                             backgroundColor:'#fc033d',
                             color:'white',
                             padding:'10px',
                             fontSize:'40px',
                         }}>
                          <h2 style={{color:'white'}}>{this.state.data.feedback ? this.state.data.feedback:0}</h2>
                          
                        </div>

                         <div style={{
                             padding:'10px',
                             color:'black',
                             backgroundColor:'white',
                             fontWeight:'800'

                         }}>
                        <p >Total No. of Feedback</p>
                        </div>
                        </div>
                    </Row>



                    </Col>
                    <Col xs={{span:2}} sm={{span:20}} md={{span:4}} lg={{span:8}}></Col>
            </Row>
                </div>
            )
        }
    }

const regCount1 = Form.create()(regCount);


export default withApollo(regCount1)

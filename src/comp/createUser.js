import gql from 'graphql-tag'
export default gql`

mutation createUser(
    $mob: String!
    $name: String!
    $email: String
    $collegeName: String
	  $courseName: String
    $age: Int
    $gender: GENDER
    $eventName: String
  ){
    createUser(input:{
      mob: $mob
      name: $name
      email: $email
      collegeName: $collegeName
      courseName: $courseName
      age: $age
      gender: $gender
      eventName:$eventName
    }){
      id
      partitionKey
      sortKey
      mob
      name
      email
      age
      gender
      qrcode
      courseName
      collegeName
    }
  }
  `


  
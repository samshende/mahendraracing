 import React,{Component} from 'react';
    import { Button, Col,Form, Input, Row, Select,Checkbox, message,Modal,Icon,InputNumber,Radio,Upload  } from 'antd'
    import './Page.css'
    import createUser from './createUser'
    import {withApollo} from 'react-apollo'

    
    const FormItem = Form.Item
    
    
     class CreateForm extends Component{
      
    
      constructor(props){
        super(props)
        this.state={
            Message:false,
            loading: false,
            isError:false,
            isSuccess: false,
            link: '',
            checked:false,
            visible:false,
            buttonD:true,
            userName:''
        }
      }
    
      enterLoading = ()=>{
        this.setState({loading:true})
    }

    onChange=(e)=>{
      // console.log('checked = ', e.target.checked);
      this.setState({
        checked: e.target.checked,
        buttonD:!this.state.buttonD
      });
    }

    handleOk = e => {
      // console.log(e);
      this.setState({
        visible: false,
      });
    };
  
    handleCancel = e => {
      // console.log(e);
      this.setState({
        visible: false,
      });
    };

    showModal=()=>{
      // console.log("open modal")
      this.setState({
        visible: true,
      });
    }

    handleSubmit=(e)=>{
        e.preventDefault();

      // this.setState({
      //           isSuccess:true,
      //       })
      //   return;

        this.props.form.validateFields((err, values) => {
            
            if(!err){

        

            // console.log('Received values of form: ', values);
            // return
            this.enterLoading()
            try{
            this.props.client.mutate({
                mutation:createUser,
                variables: {
                            mob:values.mob,
                            name:values.name,
                            email:values.email,
                            age:values.age,
                            gender:values.gender,
                            collegeName:undefined,
                            courseName:undefined,
                            eventName:"user-mr3"
                             }
            }).then(({data}) => {
            //   message.success("User Created Successfully")
                // console.log(data)
                
                if(data.createUser != null){
                //   console.log('data sumitted sucessfully');
                  message.success("Registration Successfull")
                  this.setState({
                    loading:false,
                    isError: false,
                    isSuccess: true,
                    link: data.createUser.qrcode,
                    userName:values.name

                  })
                }
                })
                .catch(err =>{
                    // console.log('in catch :: ',err)
                    this.setState({
                        loading:false,
                        isError: true
                      })
                });
           
            }
            catch(err){
                // console.log("error",err)
                this.setState({
                    loading:false,
                    isError: true
                  })
            }
            }
    
            
            else{
                console.log(err)

            }
        })
    }

   

    
    
      render()
      {
        const { getFieldDecorator, getFieldValue } = this.props.form;
        
       
    return (
        <div>

            <Row>
                <Col xs={{span:0}} sm={{span:20}} md={{span:4}} lg={{span:8}}></Col>
                    
                    <Col xs={{span:20}} sm={{span:20}} md={{span:8}} lg={{span:8}}>

                        <Row>
                        <img src={require("../img/Mahindra Racing Logo-White Text.png")} 
                         style={{  width: '272px',height:'69px',marginLeft:'15%',marginBottom:'10px'}} />
{/*                        
                        <img src={require("../img/Webp.net-resizeimage (1).png")} 
                        style={{  width: '50%',height:'auto',marginLeft:'45%',marginTop:'-10px',marginBottom:'20px'}} /> */}
                        
                        </Row>
                    </Col>
                <Col xs={{span:0}} sm={{span:20}} md={{span:4}} lg={{span:8}}></Col>
            </Row>  



        { this.state.isSuccess == false &&

         <Form   onSubmit={this.handleSubmit}>
            <div>

                <Row>
                    {/* <Col  xs={{span:24}} sm={{span:24}} md={{span:8}} lg={{span:8}} xl={{span:8}} > */}
                    <Col xs={{span:2}} sm={{span:20}} md={{span:4}} lg={{span:8}}></Col>
                    
                    <Col xs={{span:20}} sm={{span:20}} md={{span:8}} lg={{span:8}}>

                       
                        <Row>
                        <Form.Item > <span style={{color:"white"}} required="true" >Name:</span>
                            {getFieldDecorator(`name`, {
                             rules: [{
                             required: true,
                             message: "Please enter Name",
                             },
                             ]
                             })(
                            <Input placeholder="Name"  style={{ width: '100%' }} />
                             )}
                        </Form.Item>
                        </Row>


                        <Row>
                        <Form.Item > <span style={{color:"white"}} required="true" >Mobile No:</span>
                            {getFieldDecorator(`mob`, {
                             rules: [{
                             required: true,
                             message: "Please enter Mobile No",
                             },
                             {
                                validator:(rule, value, cb)=>{
                                    if( value != undefined && value != "" && (isNaN(value.trim()) || value.trim().length != 10)){
                                      cb('Please enter 10 digit number only')
                                    }
                                    cb()
                                }}
                             ]
                             })(
                            <Input placeholder="Mobile No"  style={{ width: '100%' }} />
                             )}
                        </Form.Item>
                        </Row>

                        <Row>
                        <Form.Item> <span style={{color:"white"}} required="true" >Email:</span>
                            {getFieldDecorator(`email`, {
                             rules: [{
                             required: true,
                             message: "Please enter Email",
                             },
                             {
                            
                                validator:(rule, value, cb)=>{
                                  if(value){
                                    if(!value.match( /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)){
                                      cb('Please enter valid Email address')
                                    }
                                  }
                                    cb()
                                }}
                             ]
                             })(
                            <Input  placeholder="Email"  style={{ width: '100%' }} />
                             )}
                        </Form.Item>
                        </Row>

                        <Row>
                        <Form.Item > <span style={{color:"white"}} required="true" >Age:</span>
                            {getFieldDecorator(`age`, {
                             rules: [{
                             required: true,
                             message: "Please enter Age ",
                             },
                             { validator: (rule, value, cb) => {
                                if(value && !value.match(/^[0-9]+$/g)){
                                    cb("Please enter valid Age")
                                    return;
                                }
                                    cb()
                                }
                              }
                             ]
                             })(
                            <Input placeholder="Age"  style={{ width: '100%' }} />
                             )}
                        </Form.Item>
                        </Row>

                        {/* <Row>
                        <Form.Item > <span style={{color:"white"}} required="true" >College Name:</span>
                            {getFieldDecorator(`collegeName`, {
                             rules: [{
                             required: true,
                             message: "Please enter College Name",
                             },
                             ]
                             })(
                            <Input placeholder="College Name"  style={{ width: '100%' }} />
                             )}
                        </Form.Item>
                        </Row>

                        <Row>
                        <Form.Item > <span style={{color:"white"}} required="true" >Course Name:</span>
                            {getFieldDecorator(`courseName`, {
                             rules: [{
                             required: true,
                             message: "Please enter Course Name",
                             },
                             ]
                             })(
                            <Input placeholder="Course Name"  style={{ width: '100%' }} />
                             )}
                        </Form.Item>
                        </Row> */}


                        <Row>
                        <Form.Item > <span style={{color:"white"}} required="true" >Gender:</span><br/>
                            {getFieldDecorator(`gender`, {
                             rules: [{
                             required: true,
                             message: "Please enter Gender ",
                             },
                             ]
                             })(
                                
                                    <Radio.Group>
                                    <Radio style={{color:'white'}} value={"MALE"}>Male</Radio>
                                    <Radio  style={{color:'white'}} value={"FEMALE"}>Female</Radio>
                                    </Radio.Group>
                             )}
                        </Form.Item>
                        </Row>


                        <Row>
                              <Checkbox style={{color:'white'}}
                              checked={this.state.checked}
                                onChange={this.onChange}>I agree to the &nbsp;
                                <a onClick={this.showModal}>
                                    Terms & Conditions
                                   </a>
                                   </Checkbox>
                        </Row>
                        <br/>

                        { this.state.isError == true &&
                        <p style={{color:"red"}}>*User Already Exits</p>

                        }
                        
                        <Row >
                        <Form.Item style={{}}>
                            <Button
                            type="primary"  htmlType="submit" loading={this.state.loading} 
                            disabled={this.state.buttonD}
                            style={{ marginLeft:'40%', backgroundColor:'#fc033d', color:"white",borderColor:'white' }} >
                             Submit
                            </Button>
                        </Form.Item>
                        </Row>

                        <Row>
                        <img src={require("../img/IMG_20191209_163626.JPG")} 
                        style={{  width: '100%',height:'100%'}} />

                        </Row>


                     </Col>

                    <Col xs={{span:2}} sm={{span:20}} md={{span:4}} lg={{span:8}}></Col>

                </Row>
            </div>
         </Form>
        }

    


         { this.state.isSuccess == true &&
        // <div style={{}}>

            <Row  gutter={16}>
            <Col xs={{span:2}} sm={{span:20}} md={{span:6}} lg={{span:8}}></Col>
            
           <Col xs={{span:20}} sm={{span:20}} md={{span:6}} lg={{span:8}}>
                <h3 style={{color:"white"}}>Dear {this.state.userName},</h3>
                {/* <p style={{color:"white"}}>Thank you for your Registration. Enjoy the Event</p>
                <p style={{color:"white"}}>Cheers,<br/>
                Mahindra Racing</p> */}
               <p style={{color:"white"}}> Thank you for Registration for Mahindra Racing Event. We value your support. Please find below the Racing Calendar.</p>
                <br/>
                <img src={require("../img/image.png")} 
                 style={{ marginBottom:'auto', width: '100%',height:'100%'}} />
                <br/>
           </Col>
         <Col xs={{span:2}} sm={{span:20}} md={{span:6}} lg={{span:8}}></Col>
            
         </Row>

        // </div>
         }
         <Modal
          title="Terms & Conditions"
          visible={this.state.visible}
          footer={null}
          // onOk={this.handleOk}
          onCancel={this.handleCancel}
        >
          <p>These terms  and conditions  (“the Rules”) govern  your or  your ward’sparticipation (in case of minor participants) (“Participant”/ “you”) at the“Mahindra Racing” stall (“Stall”) offered by Mahindra & Mahindra Limited(“M&M”/ “us” / “we”) at IIT Bombay during the Mood Indigo 2019 festivalfrom26 December 2019 to 29 December 2019. By participating or allowingyour ward, to participate, at the Stall you agree to be bound by these Rulesset out herein below:</p>
          <p>1.The Stall and the activities and contests at the Stall (“Games”) are solelyfor entertainment purpose and promotional purposes. We do not makeany warranty in relation to the Games or the prizes therefor and do notcommit that the experience at any of the Games will be similar to any reallife experience.</p>
          <p>2.M&M reserves the right to disqualify any individual from participating inany Game if such person is found not suitable in the sole opinion of M&M,for reasons including tampering of any property of M&M (tangible orintangible), acting in a disruptive manner, or violating the Rules.</p>
          <p>3.The Participant represents and warrants that all personal informationprovided by him/her is correct, accurate and true. The Participant agreesand acknowledges that submitting false or misleading information mayresult in disqualification from the Games.</p>
          <p>4.The Participant agrees that M&M shall hold the intellectual property rightsin   his/her   photos/images   and   videos   from   the   Stall   and   any   of   itsderivative(s) now known or later developed in any mode and form. Youhereby consent that M&M shall have a right to use such photos and videosfor any purpose as deemed fit by it including posting on social mediaplatforms for marketing and promotional activities.</p>
          <p>5.Winners of the Games will be selected solely at the discretion of the M&Mrepresentatives. This decision will be final, and no communications orcomplaints in this regard will be entertained by us, nor will we be liable toexplain the process by which the winners have been chosen. M&M is notresponsible for any disparity between the prizes received by the winners.No winner shall make any claim for a prize equal to/similar to the prizereceived by the other winner.</p>
          <p>6.M&M does not assume any responsibility for any technical errors anddisruptions during the Games.</p>
          <p>7.By providing his/her email id and mobile number the Participant agrees toreceive  promotional emails and texts relating to M&M and its groupcompanies.</p>
          <p>8.The Participant may find out about how we protect their information andhow he/she can request access to this information we hold about them,at: ______________.</p>
          <p>9.The Participant shall keep M&M, its affiliates and group companies, itsdirectors, officers, and employees, and those of its affiliates and groupcompanies, indemnified and harmless from and against any and all losses,damages,   expenses,   fees   (including   legal   fees   and   costs),   liabilitieswhether civil or criminal, arising due to any of the following:</p>
          <p>i.Any breach of these Rules by the Participant;</p>
          <p>ii.Any act or omission on part of the Participant;</p>
          <p>iii.Any   gestures,   statements   or   opinions   made   by   the   Participant   inhis/her pictures and videos; and</p>
          <p>iv.Any inaccurate, misrepresenting or misleading information provided bythe Participant to M&M.</p>
          <p>10.Limitation of Liability- We disclaim all liabilities for any damage,loss,   or   expenses   suffered   by   any   person   taking   part   at   the   Stall,attending the Mood Indigo 2019, or availing of the prize, or not being ableto take part at the Stall, or not securing a favourable outcome.</p>
          <p>11.Governing law and Dispute Resolution -The decision of M&M onall matters, queries or disputes, concerning the Stall and the Rules shallbe final and no correspondence will be entertained in this regard. Alldisputes arising out of or related to your participation at the Stall shall besubject to the exclusive jurisdiction of courts at Mumbai and subject to thelaws.</p>

        </Modal>
</div>
        )
      }
    }
    
const Login = Form.create()(CreateForm);

export default withApollo(Login)





